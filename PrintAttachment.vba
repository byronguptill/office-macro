' Written by Michael Bauer, vboffice.net
' //www.vboffice.net/en/developers/print-attachments-automatically

' use  Declare PtrSafe Function with 64-bit Outlook
Private Declare PtrSafe Function ShellExecute Lib "shell32.dll" Alias _
  "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, _
  ByVal lpFile As String, ByVal lpParameters As String, _
  ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private WithEvents Items As Outlook.Items

Private Sub Application_Startup()
  Dim Ns As Outlook.NameSpace
  Dim Folder As Outlook.MAPIFolder

  Set Ns = Application.GetNamespace("MAPI")
  Set Folder = Ns.GetDefaultFolder(olFolderInbox)
  Set Items = Folder.Items
End Sub

Private Sub Items_ItemAdd(ByVal Item As Object)
  If TypeOf Item Is Outlook.MailItem Then
    PrintAttachments Item
  End If
End Sub

Private Sub PrintAttachments(oMail As Outlook.MailItem)
  On Error Resume Next
  Dim colAtts As Outlook.Attachments
  Dim oAtt As Outlook.Attachment
  Dim sFile As String
  Dim sDirectory As String
  Dim sFileType As String
  Dim sDay As String
  Dim sTime As String
  
  Dim sFileNamePart As String
  Dim iSpacePos As Integer
  

  sDirectory = "C:\Attachments\"

  Set colAtts = oMail.Attachments

  If colAtts.Count Then
    For Each oAtt In colAtts

' This code looks at the last 4 characters in a filename



      iSpacePos = InStr(oAtt.FileName, ".")

      sFileNamePart = LCase$(Left$(oAtt.FileName, iSpacePos - 1))
      sFileType = LCase$(Right$(oAtt.FileName, 4))
      sDay = Format(Date, "dmyyy")
      sTime = Format(Time, "hhnnss")
      
      Select Case sFileType
' Add additional file types below
      Case ".pdf"
        sFile = sDirectory & sFileNamePart & sDay & sTime & sFileType
        oAtt.SaveAsFile sFile
        ShellExecute 0, "print", sFile, vbNullString, vbNullString, 0
        ShellExecute 0, "print", sFile, vbNullString, vbNullString, 0
      End Select
    Next
  End If
End Sub
